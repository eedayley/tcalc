#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define VERSION "v1.0.1"

typedef struct {
    int round;
    int displayHelp;
    int displayVersion;
} arg_flags;

void print_help() {
    char * help_message = "Usage:\n"
        "tcalc [-v -h -r][--version --help --round] commands\n\n"
        "For Example:\n"
        "$ tcalc 1+1\n"
        "2\n"
        "$ tcalc '2 * 4'\n"
        "8\n"
        "$ tcalc 20/6\n"
        "6\n"
        "$ tcalc '1 + 2 * 5'\n"
        "15\n";
    printf(help_message);
}

void print_version() {
    printf("%s\n", VERSION);
}

// function to parse the commands line args.
// It takes an array of arg strings, the number of
// args, and a pointer to a structure of flags
// If there are errors it returns 1
int parse_args(char** args, int argsCount, arg_flags* flags, char* err_msg) {
    // first initialize all struct values to 0
    flags->round = 0;
    flags->displayHelp = 0;
    flags->displayVersion = 0;
    for (int i = 0; i < argsCount; i++) {
        if (strcmp(args[i], "-r") == 0 || strcmp(args[i], "--round") == 0) {
            flags->round = 1;
        }
        else if (strcmp(args[i], "-v") == 0 || strcmp(args[i], "--version") == 0) {
            flags->displayVersion = 1;
        }
        else if (strcmp(args[i], "-h")  == 0 || strcmp(args[i], "--help") == 0) {
            flags->displayHelp = 1;
        }
        else {
            strcat(err_msg, "Unexpected Flag \"");
            strcat(err_msg, args[i]);
            strcat(err_msg, "\"\n");
            return 1;
        }
    }
    return 0;
}

// returns 1 on error
int append_char(char * dest, char src) {
    int i = 0;
    while (1) {
        if (dest[i] == 0) {
            if (dest[i+1] != 0) {
                return 1;
            }
            else {
                dest[i] = src;
                break;
            }
        }
        i++;
    }
    return 0;
}

void zero_string(char * str) {
    int i = 0;
    while (1) {
        if (str[i] == 0) {
            return;
        }
        else {
            str[i] = 0;
        }
        i++;
    }
}

int main(int argc, char ** argv) {
    if (argc == 1) {
        // no args, print usage
        print_help();
    }
    // if args, split & parse
    char * args[50] = {0}; // arguments
    int argsCount = 0; //variable to track number of arguments to program
    int parsingCommands = 0;  // variable used to track whether we are parsing commands
    char commands[1000] = {0}; // commands to be executed
    for (int i = 1; i < argc; i++) {
        if (argv[i][0] == '-' && !parsingCommands) {
            args[argsCount] = argv[i];
            argsCount++;
        }
        else {
            parsingCommands = 1;
            strcat(commands, argv[i]);
        }
    }
    // parse, check for unrecognized args
    char err_msg[50] = {0};
    arg_flags* flags;
    flags = malloc(sizeof(arg_flags));
    if (parse_args(args, argsCount, flags, err_msg)) {
        printf(err_msg);
        return 1;
    }
    // if there were flgs passed, execute the necessary actions
    if (flags->displayHelp) {
        print_help();
        return 0;
    }
    if (flags->displayVersion) {
        print_version();
        return 0;
    }
    // now perform calculations
    char num_buffer[100] = {0};
    char last_comm = ' ';
    int accumulator = 0;
    for (int i = 0; i < strlen(commands); i++) {
        if (commands[i] >= '0' && commands[i] <= '9') {
            append_char(num_buffer, commands[i]);
        }
        else if (commands[i] == ' ') {
            continue;
        }
        else {
            if (last_comm == ' ') {
                accumulator = atoi(num_buffer);
            }
            else if (last_comm == '+') {
                accumulator += atoi(num_buffer);
            }
            else if (last_comm == '-') {
                accumulator -= atoi(num_buffer);
            }
            else if (last_comm == '*') {
                accumulator *= atoi(num_buffer);
            }
            else if (last_comm == '/') {
                accumulator /= atoi(num_buffer);
            }
            else {
                printf("ERROR: unexpected character '%c'\n", last_comm);
                return 1;
            }
            last_comm = commands[i];
            zero_string(num_buffer);
        }
    }
    if (last_comm == '+') {
        accumulator += atoi(num_buffer);
    }
    else if (last_comm == '-') {
        accumulator -= atoi(num_buffer);
    }
    else if (last_comm == '*') {
        accumulator *= atoi(num_buffer);
    }
    else if (last_comm == '/') {
        accumulator /= atoi(num_buffer);
    }
    else {
        printf("ERROR: unexpected character '%c'\n", last_comm);
        return 1;
    }
    printf("%d\n", accumulator);
}
