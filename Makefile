# where to install files
prefix = /usr/local

# where the executables should be put
BINDIR=$(prefix)/bin

# VAR := $(shell echo $MANPATH | sed -r 's/^([a-zA-Z0-9\/]+)\:.+/\1/')

MANUAL = tcalc.1

build:
	mkdir -p build
	gcc main.c -o build/tcalc

install:
	mkdir -p $(DESTDIR)$(BINDIR)
	cp ./build/tcalc $(DESTDIR)$(BINDIR)/tcalc
	make install-man

install-man:
	@echo "Copying Manpage to:"
	@eval "echo $(MANPATH) | sed -r 's/^([a-zA-Z0-9\/]+)\:.+/\1\/man1/'"
	@eval "echo $(MANPATH) | sed -r 's/^([a-zA-Z0-9\/]+)\:.+/\1\/man1/' | xargs -I{} mkdir -p $(DESTDIR)/{}"
	@eval "echo $(MANPATH) | sed -r 's/^([a-zA-Z0-9\/]+)\:.+/\1\/man1/' | xargs -I{} cp $(MANUAL) $(DESTDIR)/{}"
clean:
	rm -rf build

uninstall:
	rm -f $(DESTDIR)$(BINDIR)/tcalc
	@eval "echo $(MANPATH) | sed -r 's/^([a-zA-Z0-9\/]+)\:.+/\1\/man1/' | xargs -I{} rm $(DESTDIR){}/$(MANUAL)"
