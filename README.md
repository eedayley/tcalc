# About terminal-calculator

Terminal calculator is a simple command line calculator.
It currently only supports integer inputs, although that should
change soon.

# Usage
```bash
$ tcalc 12 + 8
20
$ tcalc 3 - 9
-6
$ tcalc 6 * 5
30
$ tcalc 6 / 3
2
```
